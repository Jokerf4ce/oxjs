// Jokerf4ce 

// oxjs/dev/js/app.js



var debug = 1;

var OX = {};


OX.js = function(script){
    
    if(debug){console.log('started OX.js('+'\''+script+'\''+');');}
    
     var a = document.createElement('script');
         a.setAttribute('class','ox_scripter');
         a.setAttribute('type','text/javascript');
         a.setAttribute('rel','javascript');
         a.setAttribute('scr',script);

    document.head.appendChild(a);
    
};
OX.css = function(css){

    if(debug){console.log('started OX.css('+'\''+css+'\''+');');}

    var a = document.createElement('link');
        a.setAttribute('class','ox_styler');
        a.setAttribute('type','text/css');
        a.setAttribute('rel','stylesheet');
        a.setAttribute('href',css);

    document.head.appendChild(a);
};
OX.route = function(url){

    if(debug){console.log('started OX.route('+'\''+url+'\''+');');}

    var res = {};

    if(url){

        url = String(url);


        

        res.url     = url.split('/');
        res.uri     = res.url.splice(3);
        res.request = res.uri[0];
        res.query   = res.request.split('?')[1];
        res.php     = res.request.split('?')[0];

        return res;

    }

};
OX.Sites = {

    article : function(){

        if(debug){ console.log('started OX.Sites.article();'); }

        document.title = 'Artikel - OX';
    },
    bill : function(){

        if(debug){ console.log('started OX.Sites.bill();' ); }

        document.title = 'Aufträge - OX';
    },
    contact : function(){

        if(debug){console.log('started OX.Sites.contact();' ); }

        document.title = 'Adressen - OX';

        var le_table = document.getElementsByTagName('table')[0];
        le_table.setAttribute('id','le_table');


        var le_site = document.getElementsByTagName('table')[10];
        le_site.setAttribute('id','le_site');
        le_site.setAttribute('width','100%');

        var nodelist = [].slice.call(document.querySelectorAll('td div a'));
        var list = Array.prototype.slice.call(nodelist);
        list = list.slice(11);
        
        for( var l = 0; l<list.length; l++ ) { 
            
            list[l].setAttribute('id','ox_mailto_'+l);
            list[l].innerHTML = '';
            
            var i = document.createElement('i');
                i.setAttribute('class','fa  fa-envelope-o');
                i.setAttribute('aria-hidden','true');
                
            list[l].appendChild(i);    
            
        }

    },
    creditor : function(){

        if(debug){console.log('started OX.Sites.creditor();' ); }

        document.title = 'Kredioren - OX';
    },
    debitor : function(){

        if(debug){console.log('started OX.Sites.debitor();' ); }

        document.title = 'Debitoren - OX';
    },
    edit_contact : function(){

        var name;

        if(debug){console.log('started OX.Sites.edit_contact();' ); }

        if( document.getElementsByTagName('strong')) {
            if( document.getElementsByTagName('strong')[1] ) { name = document.getElementsByTagName('strong')[0].innerHTML + ' ' + document.getElementsByTagName('strong')[1].innerHTML; }
            else { name = document.getElementsByTagName('strong')[0].innerHTML; }
        }
        document.title = name + ' - OX';
        var le_table = document.getElementsByTagName('table')[0];
        le_table.setAttribute('id','le_table');

        var le_navi = document.getElementsByName("navi")[0];
        le_navi.setAttribute('id','le_navi');

        var div = le_navi.getElementsByTagName('div')[0];

        div.setAttribute('id','navi');

        var icons = [
            'fa fa-2x fa-angle-double-left','fa fa-2x fa-angle-double-right','fa ','fa fa-2x fa-map-marker','fa fa-2x fa-address-book-o',
            'fa fa-2x fa-address-card-o','fa fa-2x fa-area-chart','fa fa-2x fa-cube','fa fa-2x fa-cube',
            'fa fa-2x fa-cube','fa fa-2x fa-cube','fa fa-2x fa-cube','fa fa-2x fa-cube','fa fa-2x fa-cube',
            'fa fa-2x fa-check-square-o','fa fa-2x fa-file-image-o','fa fa-2x fa-exchange','fa fa-2x fa-print',
            'fa fa-2x fa-envelope-open-o','fa fa-2x fa-envelope-o','fa fa-2x fa-edit','fa fa-2x fa-sticky-note-o','fa fa-2x fa-trash-o',
        ];


        for( var n = 0; n<icons.length; n++) {

                var i = document.createElement('i');
                i.setAttribute('class',icons[n]);
                i.setAttribute('aria-hidden','true');

                var a = div.getElementsByTagName('a')[n];
                a.innerHTML = '';
                a.setAttribute('class','spacer menu-item');
                a.appendChild(i);

        }


        var le_content = le_table.getElementsByTagName('tbody')[0];
        le_content = le_content.getElementsByTagName('tr')[0];
        le_content = le_content.getElementsByTagName('td')[0];

        le_content = le_content.getElementsByTagName('table')[0];
        le_content = le_content.getElementsByTagName('tbody')[0];
        le_content = le_content.getElementsByTagName('tr')[0];
        le_content = le_content.getElementsByTagName('td')[0];

        le_content = le_content.getElementsByTagName('table')[0];
        le_content = le_content.getElementsByTagName('tbody')[0];
        le_content = le_content.getElementsByTagName('tr')[0];
        le_content = le_content.getElementsByTagName('td')[0];

        le_content = le_content.getElementsByTagName('table')[0];
        le_content = le_content.getElementsByTagName('tbody')[0];

        var le_menu = le_content.getElementsByTagName('tr')[0];
        var le_site = le_content.getElementsByTagName('tr')[4];

        le_content.setAttribute('id','le_content');
        le_menu.setAttribute('id','le_menu');
        le_site.setAttribute('id','le_site');

        le_site.getElementsByTagName('td')[0].getElementsByTagName('table')[0].setAttribute('width','100%');
        le_site.getElementsByTagName('td')[0].getElementsByTagName('table')[1].setAttribute('width','100%');

        var le_submenu = document.getElementsByClassName('SubMenu')[0];
        le_submenu.setAttribute('id','le_submenu');

    },
    faq : function(){

        if(debug){console.log('started OX.Sites.faq();' ); }

        document.title = 'FAQ - OX';
    },
    index : function(){

        if(debug){console.log('started OX.Sites.index();' ); }

        document.title = 'Dashboard - OX';
    },
    login : function(){

        if(debug){console.log('started OX.Sites.login();' ); }

        document.title = 'Login - OX';
    },
    note : function(){

        if(debug){console.log('started OX.Sites.note();' ); }

        document.title = 'Notizen - OX';
    },
    task : function(){

        if(debug){console.log('started OX.Sites.task();' ); }

        document.title = 'Aufgaben - OX';
    },
    
    rapport : function() { 
        
        if(debug){ console.log('started OX.Sites.rapport();' ); }
        document.title = 'Rapport - OX';

    },
    
    work_order : function(){

        if(debug){console.log('started OX.Sites.work_order();' ); }

        document.title = 'Arbeitsaufträge - OX';
    },


};

OX.getSite = function() {

    var q = OX.route(window.location);
    if(q){

        if(q.php){

            switch(q.php){

                default :
                break;

                case 'arbeitsauftrag.php' :
                    OX.Sites.work_order();
                break;

                case 'artikel.php' :
                    OX.Sites.article();
                break;

                case 'ccontacts.php' :
                    OX.Sites.contact();
                break;

                case 'debit.php' :
                    OX.Sites.debitor();
                break;

                case 'edit_ccontact.php' :
                    OX.Sites.edit_contact();
                break;

                case 'index.php' :
                    OX.Sites.index();
                break;

                case 'kreditor.php' :
                    OX.Sites.creditor();
                break;

                case 'login.php' :
                    OX.Sites.login();
                break;

                case 'notes.php' :
                    OX.Sites.note();
                break;

                case 'notes2.php' :
                    OX.Sites.faq();
                break;

                case 'rapport.php' :
                    OX.Sites.rapport();
                break;

                case 'tasks.php' :
                    OX.Sites.task();
                break;

                case 'rechnung.php' :
                    OX.Sites.bill();
                break;

            }

        }
    }


};

OX.init = function() {

        if(debug){console.log('started OX.init('+ '\'master/dev\'' +');');}

    OX.js('https://momentjs.com/downloads/moment.js');
    OX.getSite();

};




document.body.setAttribute('onload','OX.init();Theme.init();');


// EOF
