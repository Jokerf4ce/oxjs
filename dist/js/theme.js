// Jokerf4ce 

// oxjs/dist/js/theme.js

// minified by hand


var debug = 0;

var Theme = {};


Theme.js = function(script){
    
    if(debug){console.log('started OX.js('+'\''+script+'\''+');');}
    
     var a = document.createElement('script');
         a.setAttribute('class','ox_scripter');
         a.setAttribute('type','text/javascript');
         a.setAttribute('rel','javascript');
         a.setAttribute('scr',script);

    document.head.appendChild(a);
    
};
Theme.css = function(css){

    if(debug){console.log('started OX.css('+'\''+css+'\''+');');}

    var a = document.createElement('link');
        a.setAttribute('class','ox_styler');
        a.setAttribute('type','text/css');
        a.setAttribute('rel','stylesheet');
        a.setAttribute('href',css);

    document.head.appendChild(a);
};





function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function removeElementsByClass(className){
    var elements = document.getElementsByClassName(className);
    while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
    }
}



Theme.set = function(theme) {

    if(debug){console.log('started OX.Theme.set('+ '\''+ theme + '\'' +');');}

    var expires = new Date;
    expires.setMonth(expires.getMonth() + 3);
    var cookie = 'theme=' + theme + '; expires=' + expires + '; ' + 'path=/';

    document.cookie = cookie;
     if(debug){ console.info('document.cookie = ' + cookie ); };

    switch(theme){

        default :
            Theme.Themes[0].action();
        break;

        case 'default' :
            Theme.Themes[1].action();
        break;

        case 'zea-green' :
            Theme.Themes[2].action();
        break;


    }


};

Theme.get = function(){

    if(debug){ console.log('started OX.Theme.get();'); }

    var sel = document.getElementById('select_theme');
    var theme;

    if(sel.value) { 
        
        theme = sel.value;
        
        if(theme=='none') { 
            
            theme = getCookie('theme'); 
            sel.value = theme;
            
        }
        
    }
    
    else { theme = getCookie('theme'); }

    Theme.set(theme);
};




Theme.Themes = [


 { name : 'none',    action : function(){

     if(debug){console.log('started OX.Themes.none.action();');}


 } },
 { name : 'default',    action : function(){

     if(debug){console.log('started OX.Themes.default.action();');}
     removeElementsByClass('ox_styler');

 } },
 { name : 'zea-green',  action : function(){

     if(debug){console.log('started OX.Themes.zea-green.action();');}

            Theme.css(['https://fonts.googleapis.com/css?family=Lato']);
            Theme.css(['https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css']);
            Theme.css(['https://gl.githack.com/Jokerf4ce/oxjs/raw/master/dist/css/zea-green.css']);
    }}
];


Theme.init = function(){
    
    if(debug){console.log('started OX.Theme.init();');}
    
    
    var theme = document.createElement('div');
    theme.setAttribute('id','div_theme');

    var select = document.createElement('select');
    select.setAttribute('id','select_theme');
    select.setAttribute('style', 'float: left;');

    for(var t=0; t<Theme.Themes.length; t++){

        var option = document.createElement('option');
        option.setAttribute('id','opt_' + Theme.Themes[t].name);
        option.value = Theme.Themes[t].name;
        option.innerHTML = Theme.Themes[t].name;
        select.appendChild(option);
    }
    
    var btn = document.createElement('div');
    btn.setAttribute('id','btn_theme');
    btn.setAttribute('class','spacer');
    btn.setAttribute('onclick','Theme.get();');
    var i = document.createElement('i');
    i.setAttribute('class','fa fa-2x fa-location-arrow spacer');
    i.setAttribute('aria-hidden','true');

    btn.appendChild(i);
    
    theme.appendChild(select);
    theme.appendChild(btn);
    
    
    document.querySelectorAll('table')[0].appendChild(theme);
    
    Theme.get();

};


document.body.setAttribute('onload','OX.init();Theme.init();');


// EOF